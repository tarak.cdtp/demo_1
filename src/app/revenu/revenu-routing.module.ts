import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ImportDataComponent} from './import-data/import-data.component';
import {ListrevenuArtistComponent} from './listrevenu-artist/listrevenu-artist.component';

const routes: Routes = [
  {
    path: 'importdata', component: ImportDataComponent
  },
  {
    path: 'lr', component: ListrevenuArtistComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RevenuRoutingModule { }
