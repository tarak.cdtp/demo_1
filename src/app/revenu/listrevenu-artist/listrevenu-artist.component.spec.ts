import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListrevenuArtistComponent } from './listrevenu-artist.component';

describe('ListrevenuArtistComponent', () => {
  let component: ListrevenuArtistComponent;
  let fixture: ComponentFixture<ListrevenuArtistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListrevenuArtistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListrevenuArtistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
