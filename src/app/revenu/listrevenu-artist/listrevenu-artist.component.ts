
import {Component, OnInit} from '@angular/core';
import {Stat} from '../../classes/stat';
import {StatsService} from '../../services/stats.service';


@Component({
  selector: 'app-listrevenu-artist',
  templateUrl: './listrevenu-artist.component.html',
  styleUrls: ['./listrevenu-artist.component.css']
})
export class ListrevenuArtistComponent implements OnInit {
  single: any[];
  view: any[] = [700, 400];

  datas: Stat[] = [];
  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private statsService: StatsService) {
  }

  getstats() {
   this.datas = [
      {
        name: 'Tunisie' , value: 45
      },
     {
       name: 'Egypt' , value: 30
     },
     {
       name: 'France' , value: 20
     }
    ];
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  ngOnInit(): void {
  }

  getStatsbyservice() {
    this.datas = this.statsService.getStats();
  }

  getStatsbyWebservice() {
    this.statsService.getStatsbyAPi().subscribe(
      (data) => {
          this.datas = data;
      }, (err) => {
          alert('erreur webservice');
      }
    )
  }
}
