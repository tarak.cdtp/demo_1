import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RevenuRoutingModule } from './revenu-routing.module';
import { ListrevenuArtistComponent } from './listrevenu-artist/listrevenu-artist.component';
import { ImportDataComponent } from './import-data/import-data.component';
import { FormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';




@NgModule({
  declarations: [ListrevenuArtistComponent, ImportDataComponent],
  imports: [
    CommonModule,
    RevenuRoutingModule,
    FormsModule,
    NgxChartsModule

  ]
})
export class RevenuModule { }

