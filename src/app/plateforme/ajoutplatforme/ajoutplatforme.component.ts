import { Component, OnInit } from '@angular/core';
import {Platform} from '../../classes/platform';

@Component({
  selector: 'app-ajoutplatforme',
  templateUrl: './ajoutplatforme.component.html',
  styleUrls: ['./ajoutplatforme.component.css']
})
export class AjoutplatformeComponent implements OnInit {

  plat: Platform = new Platform() ;

  addPlatforme() {

    console.log(this.plat);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
