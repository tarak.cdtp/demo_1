import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutplatformeComponent } from './ajoutplatforme.component';

describe('AjoutplatformeComponent', () => {
  let component: AjoutplatformeComponent;
  let fixture: ComponentFixture<AjoutplatformeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutplatformeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutplatformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
