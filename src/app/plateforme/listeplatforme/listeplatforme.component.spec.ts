import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeplatformeComponent } from './listeplatforme.component';

describe('ListeplatformeComponent', () => {
  let component: ListeplatformeComponent;
  let fixture: ComponentFixture<ListeplatformeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeplatformeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeplatformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
