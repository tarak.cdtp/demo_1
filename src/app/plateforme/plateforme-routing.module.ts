import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AjoutplatformeComponent} from './ajoutplatforme/ajoutplatforme.component';
import {ListeplatformeComponent} from './listeplatforme/listeplatforme.component';
import {EditplatformeComponent} from './editplatforme/editplatforme.component';

const routes: Routes = [
  {
    path: 'addp' , component: AjoutplatformeComponent
  },

  {
    path: 'modifyp' , component: EditplatformeComponent
  },

  {
    path: 'listp' , component: ListeplatformeComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlateformeRoutingModule { }
