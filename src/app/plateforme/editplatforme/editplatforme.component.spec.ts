import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditplatformeComponent } from './editplatforme.component';

describe('EditplatformeComponent', () => {
  let component: EditplatformeComponent;
  let fixture: ComponentFixture<EditplatformeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditplatformeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditplatformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
