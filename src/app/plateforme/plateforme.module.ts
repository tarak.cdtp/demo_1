import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlateformeRoutingModule } from './plateforme-routing.module';
import { ListeplatformeComponent } from './listeplatforme/listeplatforme.component';
import { AjoutplatformeComponent } from './ajoutplatforme/ajoutplatforme.component';
import { EditplatformeComponent } from './editplatforme/editplatforme.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ListeplatformeComponent, AjoutplatformeComponent, EditplatformeComponent],
  imports: [
    CommonModule,
    PlateformeRoutingModule,
    FormsModule
  ]
})
export class PlateformeModule { }
