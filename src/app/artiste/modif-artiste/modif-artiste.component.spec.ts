import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifArtisteComponent } from './modif-artiste.component';

describe('ModifArtisteComponent', () => {
  let component: ModifArtisteComponent;
  let fixture: ComponentFixture<ModifArtisteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifArtisteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifArtisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
