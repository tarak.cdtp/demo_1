import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutAlbumComponent } from './ajout-album.component';

describe('AjoutAlbumComponent', () => {
  let component: AjoutAlbumComponent;
  let fixture: ComponentFixture<AjoutAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
