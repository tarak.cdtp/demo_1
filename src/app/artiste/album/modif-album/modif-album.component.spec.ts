import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifAlbumComponent } from './modif-album.component';

describe('ModifAlbumComponent', () => {
  let component: ModifAlbumComponent;
  let fixture: ComponentFixture<ModifAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
