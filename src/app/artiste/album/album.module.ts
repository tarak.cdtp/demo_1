import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlbumRoutingModule } from './album-routing.module';
import { ListAlbumComponent } from './list-album/list-album.component';
import { AjoutAlbumComponent } from './ajout-album/ajout-album.component';
import { ModifAlbumComponent } from './modif-album/modif-album.component';


@NgModule({
  declarations: [ListAlbumComponent, AjoutAlbumComponent, ModifAlbumComponent],
  imports: [
    CommonModule,
    AlbumRoutingModule
  ]
})
export class AlbumModule { }
