import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListetChansonComponent } from './listet-chanson.component';

describe('ListetChansonComponent', () => {
  let component: ListetChansonComponent;
  let fixture: ComponentFixture<ListetChansonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListetChansonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListetChansonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
