import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutChansonComponent } from './ajout-chanson.component';

describe('AjoutChansonComponent', () => {
  let component: AjoutChansonComponent;
  let fixture: ComponentFixture<AjoutChansonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutChansonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutChansonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
