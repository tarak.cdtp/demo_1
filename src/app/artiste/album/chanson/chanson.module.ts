import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjoutChansonComponent } from './ajout-chanson/ajout-chanson.component';
import { ListetChansonComponent } from './listet-chanson/listet-chanson.component';
import { ChansonRoutingModule } from './chanson-routing.module';



@NgModule({
  declarations: [AjoutChansonComponent, ListetChansonComponent],
  imports: [
    CommonModule,
    ChansonRoutingModule
  ]
})
export class ChansonModule { }
