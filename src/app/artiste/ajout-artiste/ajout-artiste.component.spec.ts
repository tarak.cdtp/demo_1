import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutArtisteComponent } from './ajout-artiste.component';

describe('AjoutArtisteComponent', () => {
  let component: AjoutArtisteComponent;
  let fixture: ComponentFixture<AjoutArtisteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutArtisteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutArtisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
