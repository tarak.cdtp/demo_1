import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjoutArtisteComponent } from './ajout-artiste/ajout-artiste.component';
import { ModifArtisteComponent } from './modif-artiste/modif-artiste.component';
import { ListeArtisteComponent } from './liste-artiste/liste-artiste.component';
import { ArtisteRoutingModule } from './artiste--routing.module';



@NgModule({
  declarations: [AjoutArtisteComponent, ModifArtisteComponent, ListeArtisteComponent],
  imports: [
    CommonModule,
    ArtisteRoutingModule
  ]
})
export class ArtisteModule { }
