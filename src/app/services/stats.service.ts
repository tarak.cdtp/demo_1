import { Injectable } from '@angular/core';
import {Stat} from '../classes/stat';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private http: HttpClient) { }
  getStats() {
    const datas = [
      {
        name: 'japan' , value: 105
      },
      {
        name: 'italy' , value: 90
      },
      {
        name: 'baba nowel' , value: 80
      }
    ];
    return datas;
  }

  getStatsbyAPi() {
    return this.http.get<Stat[]>('httpdldlldld');
  }
}
