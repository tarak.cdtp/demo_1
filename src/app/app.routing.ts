import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  },

      {
        path: 'p', loadChildren: () => import('./plateforme/plateforme.module').then(m => m.PlateformeModule)
      },

      {
        path: 'r', loadChildren: () => import('./revenu/revenu.module').then(m => m.RevenuModule)
      }
    ]},

  {
    path: '**',
    redirectTo: 'dashboard'
  }
]
